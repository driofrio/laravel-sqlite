<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Helpers\HttpHelper;
use Illuminate\Http\Request;

class CustomRegistrationController extends Controller
{
    private $httpHelper;
    /**
     * CustomRegistrationController constructor.
     */
    public function __construct() {
        //initialize HttpHelper
        $this->httpHelper = new HttpHelper();
    }

    /**
     * Show registration form
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showRegistrationForm() {
        return view("auth.register");
    }

    //
    public function register(Request $request) {
        try {
            $result = $this->httpHelper->post("register", [
                //insert required registration fields
            ]);


        } catch(\GuzzleHttp\Exception\ClientException $e) {
            //return back with errors
        }

        //return to login page after registration
        return redirect('/login');
    }

}