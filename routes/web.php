<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Auth::routes();


// Authentication Routes...
Route::get('login', 'Auth\CustomAuthController@showLoginForm')->name('login');
Route::post('login', 'Auth\CustomAuthController@login');
Route::post('logout', 'Auth\CustomAuthController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\CustomRegistrationController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\CustomRegistrationController@register');
// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\CustomPasswordController@showResetForm');
Route::post('password/email', 'Auth\CustomPasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\CustomPasswordController@reset');




Route::get('/home', 'HomeController@index')->name('home');
