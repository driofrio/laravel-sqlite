<?php
namespace App\Helpers;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\App;
/**
 * Class to handle all RESTful requests
 */
class HttpHelper
{
    private $guzzle;

    /**
     * HttpHelper constructor.
     */
    public function __construct()
    {
        $this->guzzle = new Client([
            'base_uri' => 'http://localhost:8020/api/v2/nubico/es/',
            'timeout' => 10,
        ]);
    }
    /**
     * @param $endpoint
     * @param $array - Array of data to be JSON encoded
     * @return mixed
     */
    public function post($endpoint, $array) {
        $response = $this->guzzle->post($this->cleanEndpoint($endpoint), $array);
//        dd($response);
        $body = json_decode($response->getBody());
        return $body->data;
    }
    /**
     * @param $endpoint
     * @param int $page
     * @return mixed
     */
    public function get($endpoint, $email, $password) {
        $ip = $this->requestIp();
        $response = $this->guzzle->get($this->cleanEndpoint($endpoint) . "?email=$email&password=$password&ip=$ip", [
            'headers' => [
                'Content-Type' => 'application/json; charset=UTF8',
                'timeout' => 10,
            ],
        ]);
        $body = json_decode($response->getBody());
        return $body;
    }
    /**
     * @param $endpoint
     * @param $array - Array of data to be JSON encoded
     * @return mixed
     */
    public function patch($endpoint, $array) {
        $response = $this->guzzle->patch($this->cleanEndpoint($endpoint), [
            'headers' => [
                'Content-Type' => 'application/json; charset=UTF8',
                'timeout' => 10,
            ],
            'auth' => [
                $this->un,
                $this->pw,
            ],
            'json' => $array
        ]);
        $body = json_decode($response->getBody());
        return $body->data;
    }
    /**
     * @param $endpoint
     * @return mixed
     */
    public function delete($endpoint) {
        $response = $this->guzzle->delete($this->cleanEndpoint($endpoint), [
            'headers' => [
                'Content-Type' => 'application/json; charset=UTF8',
                'timeout' => 10,
            ],
            'auth' => [
                $this->un,
                $this->pw,
            ],
        ]);
        $body = json_decode($response->getBody());
        return $body->data;
    }
    /**
     * Remove leading or trailing forward slashes from the endpoint.
     * @param $endpoint
     * @return string
     */
    private function cleanEndpoint($endpoint) {
        $endpoint = ltrim($endpoint,"/");
        $endpoint = rtrim($endpoint,"/");
        return $endpoint;
    }


    /**
     * Get the client IP
     * @return string
     */
    private function requestIp() {
        if (App::environment('local')) {
            return '80.28.120.204';
        } else {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            if(strpos($ip, ',') !== false){
                $explodedIp = explode(',', $ip);
                $ip = $explodedIp[0];
            }
            return trim($ip);
        }
    }
}